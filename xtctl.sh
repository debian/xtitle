#!/bin/sh -
PATH=/bin:/usr/bin; export PATH

# xtctl - partial front-end for xterm control sequences
# Steve Kinzler, kinzler@cs.indiana.edu, Mar 14
# http://www.cs.indiana.edu/~kinzler/home.html#x11

# See http://www.xfree86.org/4.8.0/ctlseqs.html
# many more control possibilities are possible to extend this script

quiet=; target=; bad=

while :
do
	case $# in
	0)	break;;
	*)	case "$1" in
		-q)	quiet=t;;
		-[fbc])	target=$1;;

		--)	shift; break;;
		-h)	bad=t; break;;
		-*)	bad=t; echo "$0: unknown option ($1)" 1>&2;;
		*)	break;;
		esac
		shift;;
	esac
done

case "$#,$bad" in
0,*|*,?*)	cat << EOF 1>&2
usage: $0 [ -q ] [ -f | -b | -c ] color
	-q	quiet mode, don't report settings
	-f	set VT100 text foreground color
	-b	set VT100 text background color
	-c	set text cursor color
EOF
		exit 1;;
esac

case "$target" in
*f*)	echo "]10;$*fg_color = $*";;
*b*)	echo "]11;$*bg_color = $*";;
*c*)	echo "]12;$*cursor_color = $*";;
esac |
case "$quiet" in
?*)	sed 's/.*//' | tr -d '\012';;
*)	cat;;
esac
